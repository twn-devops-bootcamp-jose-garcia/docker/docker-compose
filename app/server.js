let express = require('express');
let path = require('path');
let fs = require('fs');
const { MongoClient, ServerApiVersion } = require('mongodb');
let bodyParser = require('body-parser');
let app = express();

app.use(bodyParser.urlencoded({
  extended: true
}));
app.use(bodyParser.json());

app.get('/', function (req, res) {
    res.sendFile(path.join(__dirname, "index.html"));
  });

app.get('/profile-picture', function (req, res) {
  let img = fs.readFileSync(path.join(__dirname, "images/profile-1.jpg"));
  res.writeHead(200, {'Content-Type': 'image/jpg' });
  res.end(img, 'binary');
});

// use when starting application locally with node command
// let mongoUrlLocal = "mongodb://admin:password@localhost:27017";

const dbUser = process.env.MONGO_DB_USERNAME;
const dbSecret = process.env.MONGO_DB_PWD;

// use when starting application as docker container, part of docker-compose
let mongoUrlDockerCompose = 'mongodb://' + dbUser + ':' + dbSecret + '@mongodb';

// pass these options to mongo client connect request to avoid DeprecationWarning for current Server Discovery and Monitoring engine
// let mongoClientOptions = { useNewUrlParser: true, useUnifiedTopology: true };
const client = new MongoClient(mongoUrlDockerCompose, {
    serverApi: {
        version: ServerApiVersion.v1,
        strict: true,
        deprecationErrors: true,
    }
});

// "user-account" in demo with docker
let databaseName = "user-account";
let collectionName = "users";

app.get('/get-profile', async function  (req, res) {
  let response = {};
  // Connect to the db using local application or docker compose variable in connection properties
  try {
    await client.connect();

    const myDb = client.db(databaseName);
    const myColl = myDb.collection(collectionName);
    
    let myquery = { userid: 1 };

    const result = await myColl.findOne(myquery);

    res.send(result ? result : {});

  } catch (err) {
    if(err) throw err;
  } finally {
    await client.close();
  }
});

app.post('/update-profile', async function (req, res) {
  let userObj = req.body;
  // Connect to the db using local application or docker compose variable in connection properties
  try {
    await client.connect();

    const myDb = client.db(databaseName);
    const myColl = myDb.collection(collectionName);
    
    let myquery = { userid: 1 };
    let newvalues = { $set: userObj };

    await myColl.updateOne(myquery, newvalues, {upsert: true});

    res.send(userObj);

  } catch (err) {
    if(err) throw err;
  } finally {
    await client.close();
  }
});

app.listen(3000, function () {
  console.log("app listening on port 3000!");
});

